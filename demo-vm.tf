resource "random_id" "instance_id" {
  byte_length = 8
}

resource "google_compute_instance" "demo_vm" {
  name         = "vm-${random_id.instance_id.hex}"
  machine_type = "f1-micro"
  zone         = "europe-west1-b"

  boot_disk {
    initialize_params {
      image = "debian-cloud/debian-11"
    }
  }

  metadata_startup_script = <<EOT
sudo apt-get update
sudo apt-get install apache2 -y
echo "<!doctype html><html><body><h1>Hello World. Install time $(date).</h1></body></html>" | sudo tee /var/www/html/index.html
EOT

  network_interface {
    network = "default"

    access_config {
      // Include this section to give the VM an external ip address
    }
  }

  // Apply the firewall rule to allow external IPs to access this instance
  tags = ["http-server"]
}

// Note: this firewall rule is managed outside this plan 
//
// resource "google_compute_firewall" "http-server" {
//   name    = "default-allow-http"
//   network = "default"
//
//   allow {
//     protocol = "tcp"
//     ports    = ["80"]
//   }
//
//   // Allow traffic from everywhere to instances with an http-server tag
//   source_ranges = ["0.0.0.0/0"]
//   target_tags   = ["http-server"]
// }

resource "google_dns_record_set" "demo_vm" {
  name = "${google_compute_instance.demo_vm.name}.${data.google_dns_managed_zone.devops_nakednerds_net.dns_name}"
  type = "A"
  ttl  = 300

  managed_zone = data.google_dns_managed_zone.devops_nakednerds_net.name

  rrdatas = [google_compute_instance.demo_vm.network_interface.0.access_config.0.nat_ip]
}

output "ip" {
  value = google_compute_instance.demo_vm.network_interface.0.access_config.0.nat_ip
}

output "dns" {
  value = google_dns_record_set.demo_vm.name
}
