data "google_dns_managed_zone" "devops_nakednerds_net" {
  name = "devops-nakednerds-net"
}

resource "google_dns_record_set" "test_txt" {
  name = "test.${data.google_dns_managed_zone.devops_nakednerds_net.dns_name}"
  type = "TXT"
  ttl  = 300

  managed_zone = data.google_dns_managed_zone.devops_nakednerds_net.name

  rrdatas = ["hello world"]
}
